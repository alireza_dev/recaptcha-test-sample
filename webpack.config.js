const path = require('path');
const distPath = path.join(__dirname, 'public');

module.exports = [
    {
        target: "web",
        devServer: {
            contentBase: distPath ,
            host : "localhost",
            port: 85,
        },
        entry: './index.jsx',
        output: {
            filename: 'app.js',
            path: distPath,
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                }
            ]
        },
        resolve: {
            extensions: ['*', '.js','.jsx']
        }
    }
];
