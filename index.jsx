const React = require("react");
const ReactDom = require("react-dom");
const Recaptcha = require("react-google-recaptcha").default;

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            siteKey : "6LdMg-AZAAAAAOKU-AfvwBX55cI5cUmiIhDzhQND",
            generatedRecaptcha : "",
        };
    }
    render() {
        return(
            <div>
                <h5>Site Key : </h5>
                <input value={this.state.siteKey} onChange={(e)=>this.setState({ siteKey:e.target.value })}/>
                <br/>
                <Recaptcha
                    sitekey={this.state.siteKey}
                    onChange={(value)=>this.setState({generatedRecaptcha : value})}
                />
                <br/>
                <h5>Generated Recaptcha : </h5>
                <textarea value={this.state.generatedRecaptcha} disabled style={{width:300,height:200}}/>
            </div>
        )
    }
}

ReactDom.render( <App/> , document.getElementById("app-root") )